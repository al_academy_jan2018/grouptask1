#!/bin/bash

apt-get -y update
apt-get -y install nginx
systemctl start nginx
systemctl enable nginx
ip=$(ifconfig | grep "inet "|awk '{print $2}'|head -2|tail -1|tail -c 13)
echo "<!DOCTYPE html>
<html>
<body>
<p>Lily She-Yin <br/>
$ip <br/>
Ubuntu 16.04 <br/>
NGINX</p>
</body>
</html>" >/var/www/html/index.html
