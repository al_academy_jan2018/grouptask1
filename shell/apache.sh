#!/bin/bash

yum clean all
yum -y install httpd
systemctl start httpd
systemctl enable httpd
ip=$(ifconfig | grep "inet "|awk '{print $2}'|grep -v "127.0.0.1")
echo "<!DOCTYPE html>
<html>
<body>
<p>Lily She-Yin <br/>
$ip <br/>
CentOS 7.2 <br/>
Apache</p>
</body>
</html>" >/var/www/html/index.html
